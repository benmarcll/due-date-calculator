package homework.controller;

import homework.service.DueDateService;
import lombok.extern.java.Log;

import java.time.LocalDateTime;

@Log
public class DueDateController {
    DueDateService dueDateService = new DueDateService();

    public void start() {
        LocalDateTime submitDate = LocalDateTime.of(2023, 5, 16, 14, 12);
        long turnaroundTime = 16L;

        System.out.printf(
                "The submit date is: %s%n",
                submitDate
        );

        System.out.printf(
                "Turnaround time is %d hours%n",
                turnaroundTime
        );

        System.out.printf(
                "The issue is going to be resolved: %s%n",
                dueDateService.calculateDueDate(submitDate, turnaroundTime)
        );
    }
}
