package homework.service;

import java.time.*;
import java.util.List;

public class DueDateService {
    private static final List<DayOfWeek> WORKING_DAYS = List.of(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY, DayOfWeek.FRIDAY);
    private static final LocalTime START_OF_WORKING_HOURS = LocalTime.of(9, 0);
    private static final LocalTime END_OF_WORKING_HOURS = LocalTime.of(17, 0);
    private static final long HOUR_IN_MINUTES = 60;

    /**
     * @param submitDate            the date when the issue was submitted
     * @param turnaroundTimeInHours the time for that task in working hours
     * @return the date when the issue is resolved
     */
    public LocalDateTime calculateDueDate(LocalDateTime submitDate, long turnaroundTimeInHours) {
        checkParameterValidity(submitDate, turnaroundTimeInHours);

        long turnaroundTimeInMinutes = turnaroundTimeInHours * HOUR_IN_MINUTES;
        while (turnaroundTimeInMinutes > 0) {
            LocalDateTime nextHourDate = submitDate.plusMinutes(1);

            if (isItWorkingHour(nextHourDate) && isItWorkDay(nextHourDate)) {
                turnaroundTimeInMinutes--;
            }

            submitDate = nextHourDate;
        }

        return submitDate;
    }

    private boolean isItWorkDay(LocalDateTime dateTime) {
        return WORKING_DAYS.contains(dateTime.getDayOfWeek());
    }

    private boolean isItWorkingHour(LocalDateTime dateTime) {
        LocalTime time = dateTime.toLocalTime();

        return time.isAfter(START_OF_WORKING_HOURS) && !time.isAfter(END_OF_WORKING_HOURS);
    }

    /*
    I thought it is necessary because a submit date can be exactly at 09:00, so in that case
    the start of the working hour should be inclusive.
    But the other case, when I calculate the due date, a due can not be exactly at 09:00,
    because that moment when it becomes 09:00 from 08:59 it is still not working time. You can not have a due at 09:00.
    After very little, like a nanosecond or something it is working time, so that case checking the working hour should be exclusive from start.
     */
    private boolean isItWorkingHourWithInclusiveStart(LocalDateTime dateTime) {
        LocalTime time = dateTime.toLocalTime();

        return !time.isBefore(START_OF_WORKING_HOURS) && !time.isAfter(END_OF_WORKING_HOURS);
    }

    private void checkParameterValidity(LocalDateTime submitDate, long turnaroundTime) throws IllegalArgumentException {
        // Checking parameters
        if (turnaroundTime <= 0)
            throw new IllegalArgumentException(
                    String.format("Turnaround time can not be less than zero or equal to zero. Current value is: %d", turnaroundTime)
            );

        if (submitDate.isAfter(LocalDateTime.now()))
            throw new IllegalArgumentException(
                    "Submit date is after the current date. It has to be in the past."
            );
        if (!isItWorkingHourWithInclusiveStart(submitDate) || !isItWorkDay(submitDate))
            throw new IllegalArgumentException(
                    String.format(
                            "Submit date has to be workday and working hour. Currently: %s",
                            submitDate
                    )
            );

    }
}
