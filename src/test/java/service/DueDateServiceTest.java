package service;

import homework.service.DueDateService;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class DueDateServiceTest {
    DueDateService dueDateService = new DueDateService();

    @Test
    void calculateDueDate_NoMinute_NoDayShift_NoWeekShift_1H_True() {
        assertEquals(dueDateService.calculateDueDate(
                        LocalDateTime.of(2023, 5, 19, 9, 0),
                        1L
                ),
                LocalDateTime.of(2023, 5, 19, 10, 0));
    }

    @Test
    void calculateDueDate_Minute_NoDayShift_NoWeekShift_1H_True() {
        assertEquals(dueDateService.calculateDueDate(
                        LocalDateTime.of(2023, 5, 19, 9, 26),
                        1L
                ),
                LocalDateTime.of(2023, 5, 19, 10, 26));
    }

    @Test
    void calculateDueDate_NoMinute_DayShift_NoWeekShift_5H_True() {
        assertEquals(dueDateService.calculateDueDate(
                        LocalDateTime.of(2023, 5, 16, 14, 0),
                        5L
                ),
                LocalDateTime.of(2023, 5, 17, 11, 0));
    }

    @Test
    void calculateDueDate_Minute_DayShift_NoWeekShift_6H_True() {
        assertEquals(dueDateService.calculateDueDate(
                        LocalDateTime.of(2023, 5, 16, 14, 32),
                        6L
                ),
                LocalDateTime.of(2023, 5, 17, 12, 32));
    }

    @Test
    void calculateDueDate_Minute_DayShift_NoWeekShift_35H_True() {
        assertEquals(dueDateService.calculateDueDate(
                        LocalDateTime.of(2023, 5, 15, 9, 32),
                        35L
                ),
                LocalDateTime.of(2023, 5, 19, 12, 32));
    }

    @Test
    void calculateDueDate_NoMinute_DayShift_WeekShift_9H_True() {
        assertEquals(dueDateService.calculateDueDate(
                        LocalDateTime.of(2023, 5, 19, 9, 0),
                        9L
                ),
                LocalDateTime.of(2023, 5, 22, 10, 0));
    }

    @Test
    void calculateDueDate_NoMinute_DayShift_WeekShift_164H_True() {
        assertEquals(dueDateService.calculateDueDate(
                        LocalDateTime.of(2023, 5, 19, 9, 0),
                        164L
                ),
                LocalDateTime.of(2023, 6, 16, 13, 0));
    }

    @Test
    void calculateDueDate_Minute_DayShift_WeekShift_164H_True() {
        assertEquals(dueDateService.calculateDueDate(
                        LocalDateTime.of(2023, 5, 19, 16, 48),
                        164L
                ),
                LocalDateTime.of(2023, 6, 19, 12, 48));
    }

    @Test
    void calculateDueDate_DontAcceptNonWorkingDay() {
        LocalDateTime nonWorkingDateTime = LocalDateTime.of(2023,5,21,11,11);

        Exception exception = assertThrows(IllegalArgumentException.class, () -> dueDateService.calculateDueDate(
                nonWorkingDateTime,
                10L
        ));

        String expectedMessage = String.format(
                "Submit date has to be workday and working hour. Currently: %s",
                nonWorkingDateTime
        );
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void calculateDueDate_DontAcceptNoWorkHour() {
        LocalDateTime noWorkHourDateTime = LocalDateTime.of(2023,5,16,6,11);

        Exception exception = assertThrows(IllegalArgumentException.class, () -> dueDateService.calculateDueDate(
                noWorkHourDateTime,
                10L
        ));

        String expectedMessage = String.format(
                "Submit date has to be workday and working hour. Currently: %s",
                noWorkHourDateTime
        );
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void calculateDueDate_DontAcceptFutureSubmitDate() {
        LocalDateTime futureDateTime = LocalDateTime.of(2025,5,16,6,11);

        Exception exception = assertThrows(IllegalArgumentException.class, () -> dueDateService.calculateDueDate(
                futureDateTime,
                10L
        ));

        String expectedMessage = "Submit date is after the current date. It has to be in the past.";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void calculateDueDate_DontAcceptZeroTurnaroundTime() {
        LocalDateTime futureDateTime = LocalDateTime.of(2022,5,16,6,11);
        long turnaroundTime = 0L;

        Exception exception = assertThrows(IllegalArgumentException.class, () -> dueDateService.calculateDueDate(
                futureDateTime,
                turnaroundTime
        ));

        String expectedMessage = String.format("Turnaround time can not be less than zero or equal to zero. Current value is: %d", turnaroundTime);
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void calculateDueDate_DontAcceptNegativeTurnaroundTime() {
        LocalDateTime futureDateTime = LocalDateTime.of(2022,5,16,6,11);
        long turnaroundTime = -5L;

        Exception exception = assertThrows(IllegalArgumentException.class, () -> dueDateService.calculateDueDate(
                futureDateTime,
                turnaroundTime
        ));

        String expectedMessage = String.format("Turnaround time can not be less than zero or equal to zero. Current value is: %d", turnaroundTime);
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }
}
